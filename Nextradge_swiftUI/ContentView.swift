//
//  ContentView.swift
//  Nextradge_swiftUI
//
//  Created by 熊壮壮 on 2023/12/06.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        ZStack {
            LinearGradient(gradient: Gradient(colors: [.blue, .white]),
                           startPoint: .topLeading,
                           endPoint: .bottomTrailing)
            .edgesIgnoringSafeArea(.all)
            VStack {
                NameView(Name: "Nextradge")
                ImageView()
            }
        }
    }
}

struct NameView: View {
    var Name: String
    
    var body: some View {
        Text(Name)
            .font(.system(size: 32, weight: .medium, design: .default))
            .foregroundColor(.white)
            .padding(.bottom, 32)
    }
}

struct ImageView: View {
    
    var body: some View {
        VStack(spacing: 12) {
            HStack {
                Image(systemName: "apps.iphone")
                    .renderingMode(.original)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                .frame(width: 180, height: 180)
            }
            .padding()
            
            Text("APP開発")
                .font(.system(size: 32, weight: .medium))
                .foregroundColor(.white)
        }
    }
}
#Preview {
    ContentView()
}
