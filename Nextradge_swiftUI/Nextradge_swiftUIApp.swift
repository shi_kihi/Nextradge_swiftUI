//
//  Nextradge_swiftUIApp.swift
//  Nextradge_swiftUI
//
//  Created by 熊壮壮 on 2023/12/06.
//

import SwiftUI

@main
struct Nextradge_swiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
